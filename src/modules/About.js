import React, {Component} from 'react'
import { Segment, Image, Container, Header, Divider } from 'semantic-ui-react'

class About extends Component {
  render() {
    return (
      <Container text textAlign='left'>
        <Segment>
            <Header as='h2'>About</Header>
            <Divider />
            <Image src='images/Profile_Picture.png' alt="Roy Springer" size='small' floated='right' />
            <p>Ik ben Roy Springer, een 25-jarige game programmeur uit Leiden. Ik ben na de HAVO begonnen aan een studie informatica aan de Hogeschool Leiden, maar vond pas echt mijn roeping bij de Hogeschool van Amsterdam. Daar heb ik uiteindelijk mijn HBO diploma voor de studie Informatica behaald, met een specializatie in Game Technology.</p>
            <p>Tijdens mijn opleiding leerde ik over zowel software als games programmeren. Ik heb hiermee goede kennis opgedaan van verschillende programmeertalen: Java, ActionScript, C#, C++ en C. Ik heb ook ervaring gekregen met werken in projecten en onder andere met de SCRUM methode. Voor mijn afstudeerproject heb ik mij verdiept in SCRUM en hoe 'gamification' SCRUM zou kunnen ondersteunen. Tijdens mijn opleiding heb ik mijn twee praktijkstages gelopen bij het Amsterdamse bedrijf Flavour. Daar heb ik voltijd meegedraaid in de productie en veel geleerd over de ins en outs van games programmeren. Ook deed ik bij veel verschillende projecten ervaring op met het ontwikkelingsproces van begin tot einde meemaken.</p>
            <p>Naast deze twee stages, die in totaal een jaar duurden, heb ik bij Flavour part-time gewerkt naast mijn opleiding. In deze periode heb ik onder andere gewerkt aan het project MediaMasters en aan verschillende multiplayer Flash games. De front-end en back-end van deze games waren hierbij voornamelijk mijn verantwoordelijkheid.</p>
            <p>Na het afstuderen ging ik full-time bij Flavour aan de slag, waarbij ik bleef werken aan hun 2D, veelal serious games.Naast het programmeren van games, vind ik het ook leuk om aan de back-end van games te werken, PHP en MySQL. Ook heb ik interesse in de beveiliging van software en games. In de toekomst zou ik mezelf willen onwikkelen op verschillenden gebieden, onder andere hoe 3D gaming in elkaar zit, en wellicht security in software.</p>
        </Segment>
      </Container>
    )
  }
}

export default About;
