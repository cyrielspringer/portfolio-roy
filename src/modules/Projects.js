import React, {Component} from 'react';
import { Segment, Container, Header, Divider } from 'semantic-ui-react';

import Project from './Project';
import ProjectsJSON from '../storage/projects.json';

class Projects extends Component {
  static contextTypes = {
      router: React.PropTypes.object.isRequired
  };

  cloneChildren() {
    var path = this.props.location.pathname;
    if (this.props.children) {
      return React.cloneElement(this.props.children, { key: path })
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    const userName = event.target.elements[0].value;
    const project = event.target.elements[1].value;
    const path = `/projects/${userName}/${project}`;
    this.context.router.push(path)
  }

  createProjects() {
    let projects = ProjectsJSON.projects;
    let projectComponents = [];
    for (var projectKey in projects) {
      if (projects.hasOwnProperty(projectKey)) {
        let project = projects[projectKey];
        projectComponents.push(
          <div key={projectKey + '_div'}>
          <Project key={projectKey} {...project} />
          <Divider hidden section key={projectKey + '_divider'} />
          </div>
        );
      }
    }
    return projectComponents;
  }

  render() {
    return (
      <Container text textAlign='left'>
        <Segment>
            <Header as='h2'>Projects</Header>
            <Divider />
            {this.createProjects()}
        </Segment>
      </Container>
    )
  }
}

export default Projects;
