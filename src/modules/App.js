import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from '../components/Header.js';
import { Menu, Container} from 'semantic-ui-react';
import { Projects } from './Projects';

class App extends Component {

  static contextTypes = {
      router: React.PropTypes.object.isRequired
  };

  constructor(props, context) {
    super(props, context);
    this.activeItem = '';
  }

  cloneChildren() {
    var path = this.props.location.pathname;
    if (this.props.children) {
      return React.cloneElement(this.props.children, { key: path })
    }
    else {
      return (
        <Projects key={path}/>
      )
    }
  }

  handleItemClick(event, name) {
    if(name === 'projects') name = '';
    let route = `/${name}`;
    this.activeItem = route;
    this.context.router.push(route);
  }

  isActive(name) {
    if(name === 'projects') name = '';
    try {
      let paths = this.props.location.pathname.split('/');
      let currentPath = '';
      if(paths.length > 1) {
        currentPath = paths[1];
      }
      if(name === currentPath.toLowerCase()) {
        return true;
      }
      return false;
    }
    catch(e) {
      console.log(e);
      return false;
    }
  }

  render() {
    return (
      <div className="App">
        <Header title="Roy Springer" subtitle="portfolio" logo={logo}></Header>

        <Menu pointing secondary>
          <Container text>
            <Menu.Item name='projects' active={this.isActive('projects')} onClick={(e, {name}) => {this.handleItemClick(e, name)}} />
            <Menu.Item name='about' active={this.isActive('about')} onClick={(e, {name}) => {this.handleItemClick(e, name)}} />
            <Menu.Item name='contact' active={this.isActive('contact')} onClick={(e, {name}) => {this.handleItemClick(e, name)}} />
          </Container>
        </Menu>

        {this.cloneChildren()}

      </div>
    );
  }
}

export default App;
