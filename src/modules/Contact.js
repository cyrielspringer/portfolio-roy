import React, {Component} from 'react'
import { Segment, Container, Header, Divider, List, Icon } from 'semantic-ui-react'

class Contact extends Component {
  render() {
    return (
      <Container text textAlign='left'>
        <Segment>
            <Header as='h2'>Contact</Header>
            <Divider />
            <p>Wil je in contact komen met mij? Of misschien wel verschillende code snippets van mij zien? Dat kan! Je kan mij bereiken via mail of andere sociale media, klik op een van de onderstaande icons.</p>
            <List divided horizontal>
              <List.Item as='a' href='mailto:contact@royspringer.nl'>
                <Icon name='mail' size={'big'} />
              </List.Item>
              <List.Item as='a' href='http://nl.linkedin.com/in/rcspringer' target="_blank">
                <Icon name='linkedin' size={'big'} />
              </List.Item>
            </List>
        </Segment>
      </Container>
    )
  }
}

export default Contact;
