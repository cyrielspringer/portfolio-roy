import React, {Component} from 'react';
import { Header, Divider, Image, Card, Segment} from 'semantic-ui-react'

class Project extends Component {

  renderContent(content) {
    return content.split('\n').map((content, i) => {
      return <p key={i}>{content}</p>
    });
  }

  render() {
    return (
      <Segment basic clearing>
        <Header as='h3'>{this.props.title}</Header>
        <Divider />
        <Card style={{float: 'right'}}>
          <Image src={this.props.image} />
          <Card.Content>
            <Card.Header>
              Klant: {this.props.customer}
            </Card.Header>
            <Card.Meta>
              <span className='date'>
                Jaar: {this.props.year}
              </span>
            </Card.Meta>
            <Card.Description>
              Website: <a href={this.props.website}>{this.props.website}</a>
            </Card.Description>
          </Card.Content>
        </Card>
        {this.renderContent(this.props.content)}
      </Segment>
    );
  }
}

export default Project;
