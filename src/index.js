import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import '../libraries/semantic/dist/semantic.css';
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import App from './modules/App';
import About from './modules/About';
import Contact from './modules/Contact';
import Projects from './modules/Projects';
import Project from './modules/Project';

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Projects}/>
      <Route path="/projects" component={Projects}>
        <Route path="/projects/:userName/:repoName" component={Project}/>
      </Route>
      <Route path="/about" component={About} />
      <Route path="/contact" component={Contact} />
    </Route>
  </Router>,
  document.getElementById('root')
);
