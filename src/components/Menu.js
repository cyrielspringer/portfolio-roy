import React, { Component } from 'react';
import MenuItem from './MenuItem';
import './Menu.css';

class Menu extends Component {

  getMenuItems() {
    let items = [];
    let menuItems = this.getMenuConfig().menuItems;
    let i = 0;
    for (var menuItem in menuItems) {
      if (menuItems.hasOwnProperty(menuItem)) {
        let currentItem = menuItems[menuItem];
        let link = '';
        if(currentItem.link) {
          link = currentItem.link.replace(/[^a-zA-Z]/g, '').toLowerCase();
        }
        items.push(
          <MenuItem key={i} name={currentItem.name} link={link}></MenuItem>
        );
        i++;
      }
    }
    return items;
  }

  getMenuConfig() {
    return this.props.config;
  }

  getMenuClasses() {
    let config = this.getMenuConfig();
    let classes = '';
    if(config.topNav) classes += 'top-nav ';
    if(config.sideNav) classes += 'side-nav ';
    if(config.mobileNav) classes += 'mobile-nav ';
    if(config.responsive) classes += 'responsive ';
    return classes;
  }

  render() {
    return(
      <div id={this.props.id} className="o-menu">
        <ul className={"o-menu-list " + this.getMenuClasses()} role="menu">
          {this.getMenuItems()}
        </ul>
      </div>
    );
  }
}

export default Menu;
