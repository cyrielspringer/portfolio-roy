import React, { Component } from 'react';
import NavLink from './NavLink';

class MenuItem extends Component {

  render() {
    return(
        <li>
          <NavLink activeClassName="active" to={"/" + this.props.link} onlyActiveOnIndex={this.props.link === ""}>{this.props.name}</NavLink>
        </li>
    );
  }
}

export default MenuItem;
